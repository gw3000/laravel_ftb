
# Composer
To install Laravel you need to have composer. Composer is a tool for dependency
management in PHP. It allows you to declare the libraries your project depends
on and it will manage (install/update) them for you.
(as at 2021-08)

# Installation of composer

Get it from: https://getcomposer.org/download/

## Windows
Download it from the website (see above).

## Linux or MacOS
```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

php composer-setup.php

php -r "unlink('composer-setup.php');"
```

# Install the Laravel Installer

To install laravel globally type the following into the command line:

```bash
composer global require laravel/installer
```

# Laravel in PATH

To call laravel from the commandline, you have to add the following to your .bashrc/.zshrc file:

```
export PATH="$HOME/.composer/vendor/bin:$PATH"
```

After that you have to source the rc file:

```bash
source ~/.bashrc
```
