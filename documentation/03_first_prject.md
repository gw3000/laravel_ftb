# The First Project

There several ways to start a new project. You can use laravel or composer to do this.

## Using Laravel
To start a new project with the laravel command just type:

```bash
laravel new firstproject
```

## Using Composer
To start a new project by using the composer command type:

```bash
composer create-project --prefer-dist laravel/laravel firstproject
```

# Artisan

Artisan is the commandline interface of laravel. For instance you can create classes with artisan without writing a single line of code.

#  zsh and oh-my-zsh

To customize your terminal you can use this github repo:

https://gist.github.com/kevin-smets/8568070

# Serve the development site

To start the development-server go to your commandline enter the path of your firstproject and type:

```bash
php artisan serve
```


If you are using zsh and installed the oh-my-zsh extension use the laravel plugin. Then you can use the alias

```bash
pas
```

... to run the development server.

## another port

If you want to use a different port using the development server, you have to use an argument with artisan.

```bash
php artisan serve --port 8081
```

# Update all packages

To update all packages in your laravel project remove the composer.lock file and install or update the project by executing:

```bash
composer install
```

# Composer and Packagist

If you want to add libraries to your project use packagist:

https://packagist.org/

## datatables

if you want to use the datatables library in your project search after datatables in packagist. It suggests one single line to install it to the project:

```bash
composer require datatables/datatables
```

Necessary libraries are installed automatically!

# Package dependency tree

Visualize the package dependency tree type this:

```bash
composer show --tree
```
