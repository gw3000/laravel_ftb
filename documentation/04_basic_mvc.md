# Basic Routing

in the routes folder->web.php add a simple users route that returns 

- String:
```php
Route::get('users/{id}', function ($id) {
    if ($id == 1) {
        $username = "Herr Guntler";
    } else {
        $username = "Not Herr Guntler";
    }
    return 'users route for ' . $username . '!';
});
```

- Array
```php
Route::get('languages', function () {
    return ['PHP', 'HTML', 'Laravel'];
});
```

- JSON-Object:
```php
Route::get('courses', function () {
    return response()->json([
        'name'=>'Herr Guntler',
        'course'=>'How to play guitar very slow'
    ]);
});
```

- Function:
```php
Route::get('function/', function () {
    return redirect('/');
});
```

# Views

To return a view from a request you need your route.

```php
Route::get('', function () {
    return view('home');
});
```

... and your view! You have to add a new file in the resources->views folder and a home.blade.php file.

```
resources
├── css
│   └── app.css
├── js
│   ├── app.js
│   └── bootstrap.js
├── lang
│   └── en
│       ├── auth.php
│       ├── pagination.php
│       ├── passwords.php
│       └── validation.php
└── views
    ├── home.blade.php
    └── welcome.blade.php
```

# Controllers

Controllers of the laravel framework are in the app->Http->Controllers folder:

```
app
│
├── Http
│   ├── Controllers
│   │   └── Controller.php

```
## Create a new controller

To create a new controller use artisan.

```bash
php artisan make:controller ProductsController
```

Artisan created the following:

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    //
}

```

# Using the Controller

in your webroute add the Productscontroller:

```php
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;


```

... and use the Controller:

```php
Route::get('products', [ProductsController::class, 'index']);
```

... or another way to write down a route in laravel 8:

```php
Route::get('products','App\Http\Controllers\ProductsController@index');
```

- products -> the route
- ProductsController::class -> the controller
- index -> the method of the class

## The Index Method

In the Products Controller add in the class the index method:

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(){
        return view('products');
    }
}
```

To call the products view, you have to add the blade template in the resources->views folder.

```
resources
└── views
    ├── home.blade.php
    ├── products.blade.php
    └── welcome.blade.php
```

# Passing (multiple) Data to Views
In the controllers index method you can create a variable $title and send it by using the with method or compact method to the view.
Keep in mind that the with method can only use one variable! The compact method is able to carry multiple variables. The with method is able to transfer arrays to send multiple values with it

```php
public function index()
    {
        $title = "Welcome to the Jungle";
        $description = "Slash is an Idiot";
        $data = array('title' => 'Romeo and Juliet', 'description' => 'Song by Mark Knopfler');
        
        // with method is used to send one variable
        return view('products/products')->with('data', $data);

        // send multiple variables using the compact method
        return view('products/products', compact('title'));
    }
```

... and the variable to the view:
```php
</head>
<body>
    <h1>Products</h1>
    
    // compact transfer
    <p>{{ $title }}</p>
    <p>{{ $description }}</p>

    // with transfer
    @foreach ($data as $item)
        <p>{{ $item }}</p>
    @endforeach

</body>
</html>
```
