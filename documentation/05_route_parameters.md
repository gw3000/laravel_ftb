# Route Parameters

What we've done so far was doing a request to a single page. Now we want to focus on route params. Sometimes you want to pass in some kind of data to a specific file to render dynamic data.

Whenever a user does a request to products, the system returns all products.

```
Request: /products->all products
```

The system should also return specific data something like this:

```
Request: /products/productName->...
Request: /products/productID->...
```

So lets create a show route:

```php
Route::get('products/{id}',[ProductsController::class, 'show'])->where('id', '[0-9]+');
```

## Pattern is an integer
To check the param values do this with the <b>where method</b> like this:

```php
->where('id', '[0-9]+')
```

## Pattern is an string
If the pattern is a string then the where method should look like this if you want to use uppercase and lowercase string:

```php
->where('id', '[a-zA-Z]+')
```

In this case, values can only be numeric values, no alpha or alphanumeric values! <b>'[0-9]+'</b> is a regular expression.

... and the controller (-method):

```php
public function show($id){
        $data = array('1' => 'Zauberwürfel', '2'=>'Nintendo Switch');
        $toy=$data[$id] ?? 'Das Spielzeug mit der ID:' . $id . ' ist nicht vorhanden!';
        return view('products/toy')->with('toy', $toy);
    }
```

... and the view (toy.blade.php):

```php
<body>
    <h1>Spielzeug</h1>
    {{ $toy }}
</body>
```

## Multiple Pattens

If you're endpoint should look like.

```
../name/id -> ../iphone/12
```

... then your Route could look like this:

```php
Route::get('products/{name}/{id}', [ProductsController::class, 'showm'])->where([
    'name' => '[a-z]+',
    'id' => '[0-9]+'
]);
```

You can check the multiple pattern in the where method with an array:

```php
where([
    'name' => '[a-z]+',
    'id' => '[0-9]+'
]);
```
