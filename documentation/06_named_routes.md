# Named Routes

The routes in the web.php file can be named. This may look like this:

```php
Route::get('products', [ProductsController::class, 'index'])->name('products');
```

To use the route, you can use the name in your Controller with **route('name_of_the_route')**:

```php
public function index(){
        print_r(route('products'));
        return view('products.index');
    }
```

... or anywhere else. Maybe  as a link in your view:

```php
<a href="{{ route('products') }}">Products</a>
```