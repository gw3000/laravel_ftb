# Views

You don't want to repeat yourself on multiple websites when you need headers and footer that are always the same.

In your views folder (resources->views) create a folder called layouts. In that folder create a file called
'app.blade.php':

```
.
├── about.blade.php
├── index.blade.php
└── layouts
    └── app.blade.php

```

In the app.blade.php place your entire website with header and footer in it. and remove everything between the header and the footer of the body. In between at a **yield** section with a decent name (like 'content).

The structure of you app.blade.php should look like this (layouts/app.blade.php)

```php
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    ...
</head>

<body>
    <!-- Header -->
    <header>
     ...
    </header>

    <!-- Content -->
    @yield('content')

    <!-- Footer -->
    <footer>
     ...
    </footer>
</body>
</html>

```

Your original index page should be trimmed like (index.blade.php):

```php
@extends('layouts.app')
@section('content')

... Your fancy content

@endsection

```

You can go further and structure your app template like this:

```php
<html lang="en">
<head>
    ...
</head>

<body>

    @include('layouts.header')
    @yield('cotent')
    @include('layouts.footer')


</body>
</html>

```

Before that you have to put the header and the footer in a separate file (header.blade.php and footer.blade.php) in the layouts folder:

```
.
├── app.blade.php
├── footer.blade.php
└── header.blade.php
```

## Images

To store static content like (background-) images add a folder to the public folder called images:

```
.
├── favicon.ico
├── images
├── index.php
├── robots.txt
└── web.config
```

## Accessing the static content

To link the images to your blade file, go to your index.blade.php and add something like this:

```php
<img src="{{ URL('images/icon-box.jpg') }}" alt="">
```

## create symbolic links
To create symbolic links in artisan please type in the command-line:

```
php artisan storage:link
```

The output should look like this:

```
The [C:\Users\weissenbaeck\Desktop\laravel_workspace\laravel_gws\secondproject\public\storage] link has been connected to [C:\Users\weissenbaeck\Desktop\laravel_workspace\laravel_gws\secondproject\storage\app/public].
The links have been created.
```

A symbolic link has been created from the public->storage folder to the app->public folder.

## Asset and URL

To link images the alternative way you can use the **asset** method

The URL-Method:

```
{{ URL('images/icon-box.jpg') }}
```

The asset-method:
```
{{ asset('images/icon-box.jpg') }}
```
 The asset-method is specifically used to include css, js and img files. The files need to be stored inside the public folder. It's the only place the asset-method looks inside.

 ## Blade Templating Engine

 https://laravel-guide.readthedocs.io/en/latest/blade/
