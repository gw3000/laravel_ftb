# How to compile assets

To include the Javascript and CSS you need to compile it for the framework.

## Webpack Mix

In your root directory should be a file called **webpack.mix.js**. This is
basically a file  that you can copy from project to project. The file provides
a clean, fluent API for defining some Webpack build steps for your Laravel app.
By default, you are compiling the CSS file for the application as well as
bundling up all the JS files.

By the default the file looks like this:

```js

const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);
```

## From To

You will compile the Javascript stuff from:

resources/js/app.js->public/js

.. and the CSS:

resources/css/app.css->public/css

## SCSS

To compile SCSS stuff just modify your webmix file like this:
```js

const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/scss/app.scss', 'public/css', [
        //
    ]);
```
## Setup the tools
Mix runs on webpack and in order to run webpack you need to have node and npm
installed on your system. In the root of your fancy project just type:

```bash
npm install
```

... after the routine installed several dependencies, run the command again to
compile your stuff.

## The Process

Before you make it work you need to perform one more command:

```bash
npm run dev
```
... to run your webpack once.
After that you will get the message **Compiled Successfully in xxx ms**.

The file structure in your public folder should now look like this:
```
.
├── css
│   └── app.css
├── favicon.ico
├── index.php
├── js
│   └── app.js
├── mix-manifest.json
├── robots.txt
├── storage -> ..../secondproject/storage/app/public
└── web.config
```

to make webpack watch on changed files and compiling it immediately if there
saved run the npm command with the watch argument:

```bash
npm run watch
```

## Frontend presets

In laravel it's possible to use frontend presets. In combination with artisan
you can import frontend libraries such as tailwind or bootstrap with laravel.

https://github.com/laravel-frontend-presets?language=blade

To use tailwind you have to import the preset with:

```bash
composer require laravel-frontend-presets/tailwindcss --dev
```

To use tailwind in laravel type:

```bash
php artisan ui tailwindcss
```

After that type:

```bash
npm install && npm run dev
```

This will take a second to download the needed packages.
