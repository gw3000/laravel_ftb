# Databases and Migrations

To do dynamic stuff with laravel you have the database tool **eloquent**. It's
one of the most important tools to work with databases. Eloquent is an **ORM**
and Object Relational Mapper. It allows you to write queries in the object
oriented paradigm.

## The credentials

In your project route directory there is a folder config folder with a database
file in it:

```
../secondproject/config/
...
├── database.php
...
```

The database variables are set in the **.env** file in the root of your project
directory.

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=secondproject
DB_USERNAME=root
DB_PASSWORD=
```

## Migrations

After you installed your database server (mariadb) and set up a database
(db_second) you have to make a migration. A migration is something like a
version control of your database. You can run a migration right inside your
commandline. Before you can do that, you have to setup your model and your
controller.

```bash
php artisan make:controller PostsController
```

In the created PostsController add a new method called index:

```php
public function index(){
    return view('posts/index');
}
```

Add a new folder called posts in the resources->views folder and add a new file
called index.blade.php:

```bash
resources/views/
├── posts
│   └── index.blade.php
```

You have also to create a specific route in the web.php file:


```php
use App\Http\Controllers\PostsController;
...
Route::get('posts', [PostsController::class, 'index']);
```

## MVC ... The model

To use the database and its tables you have to create a model (it has to be
singular).

```bash
php artisan make:model Post
```

In the app->Models folder should be a file called **Post.php**.


```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
}
```

## The first Migration

To make a migration just type:

```bash
php artisan make:model Post -m
```

The migrations are stored in the migrations folder inside of the database
folder in the root of your project:

```bash
database/migrations/
├── 2014_10_12_000000_create_users_table.php
├── 2014_10_12_100000_create_password_resets_table.php
└── 2019_08_19_000000_create_failed_jobs_table.php
```

## Up and Down

Inside your migration file you have an up and a down method. Whenever you run
the migrate command it will look for the earliest date of your migration and it
will run the up-method. You can also roll back your migration with the
down-method.

## The first table

You need to have posts table that should have an id, title, body and timestamps
column. The up method could look like this:


```php
public function up()
{
    Schema::create('posts', function (Blueprint $table) {
        $table->increments('id');
        $table->string('title');
        $table->mediumText('body');
        $table->timestamps();
    });
}
```

To generate the table just execute:

```bash
php artisan migrate
```

The table now looks like this:

```
MariaDB [db_second]> show columns from posts;
+------------+------------------+------+-----+---------+----------------+
| Field      | Type             | Null | Key | Default | Extra          |
+------------+------------------+------+-----+---------+----------------+
| id         | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
| title      | varchar(255)     | NO   |     | NULL    |                |
| body       | mediumtext       | NO   |     | NULL    |                |
| created_at | timestamp        | YES  |     | NULL    |                |
| updated_at | timestamp        | YES  |     | NULL    |                |
+------------+------------------+------+-----+---------+----------------+
```

To remove the table just type:

```bash
php artisan migrate:resest
```

To roll back all migrations down and up (empty all tables) type:
```bash
php artisan migrate:refresh
```

## Create fake data to fill the tables

Inside you database folder there is a factories folder. Inside that folder is
file called **UserFactory.php**

To have the same with the Posts Controller type the following in the commandline:

```bash
php artisan make:factory PostsFactory --model=Post
```

In the factories folder should be a **PostFactory.php** file.

From the UserFactory.php file copy the return array and past it to the PostsFactory.php file.

```php
return [
	'name' => $this->faker->name(),
	'email' => $this->faker->unique()->safeEmail(),
	'email_verified_at' => now(),
	'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
	'remember_token' => Str::random(10),
];
```

And customize it to your requirements:


```php
return [
	'title' => $this->faker->title;
	'body' => $this->faker->paragraph;
];
```

and use another import:
```php
use Illuminate\Support\Str
```

and fit the right path to the $model variable
```php
protected $model = \App\Models\Post::class;
```

## Tinker

To generate fake data use **tinker**:
```bash
php artisan tinker
```

This will open a Psy Shell. From here set the entire path to the model:

```bash
\App\Models\Post::factory()->create();
```

To create a specific number of fake date rows use the count-method:

```bash
\App\Models\Post::factory()->count(43)->create();
```

To create a specific number of fake data rows (i.e. 43) use the count-method:

```bash
\App\Models\Post::factory()->count(43)->create();
```
