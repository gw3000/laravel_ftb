# Query Builder

In the core of every laravel database there is a functionality called the query
builder, which is fluent interface for interacting on different types of
databases with a single API.

A none fluent query:
```php
DB::select(['table'=>'post', 'where'=>['id'=>1]]);
```
A fluent query:
```php
DB:table('posts')->where('id',1)i->get();
```

In your PostsController add a none fluent query like this:

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index(){
        // fetch the post with the id 7
        $posts = DB::select('select * from posts where id = ?', [7]);

        // or a named prepared statement
        $posts = DB::select('select * from posts where id = :id', ['id'=>7]);
        // dd = dump and die
        dd($posts);
    }
}
```
What you have done so far has *nothing* to do with query builders.

# Fluent Query

Now let's create a fluent query:

```php
public function index(){
    $id = 7;
    $posts = DB::table('posts')->where('id', $id)->get();
    dd($posts);
}
```

## Select
Selecting data from a table:
```php
$posts = DB::table('posts')
    ->select('body', 'created_at')
    ->limit(10)
    ->orderBy('id', 'desc')
    // ->where('created_at', '<', now()->subDay())
    // ->orWhere('title', 'Prof.')
    // ->whereBetween('id', [7, 9])
    ->whereNotNull('title')
    ->get();
```

## Insert
Inserting data to a table:
```php
$posts = DB::table('posts')
    ->insert([
        'title'=>'New Post',
        'body'=>'New Body'
    ]);

```

## Update
Update data of a dataset:
```php
$posts = DB::table('posts')
    ->where('id', '=', 15)
    ->update([
        'title'=>'updated title',
        'body'=>'updated body'
    ]);
```

## Delete
To delete a row in a table:
```php
$posts = DB::table('posts')
    ->where('id', '=', 13)
    ->delete();
```

