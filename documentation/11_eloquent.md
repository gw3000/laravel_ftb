# Eloquent

Eloquent is an ORM (Object Relational Mapper). It's an Database
Abstraction layer that provides a single interface to interact witch a
variety of database types. A single Eloquent class is responsible for
not only providing the ability to interact with the table as row it is
also representing an individual table row.

The focus of eloquent is simple Simplicity.

## A new Project

At first make a new project (i.e. thirdproject):
```bash
laravel new thirproject
```

Make tailwind a laravel-preset:

```bash
composer require laravel-frontend-presets/tailwindcss --dev
```

Make a model and a migration:
```bash
php artisan make:model Car -m
```

Make a controller:
```bash
php artisan make:controller CarsController --resource
```

The resource flag is for CRUD responsibility!

Create your route with the **resource**-method (don't forget to use your
CarsController):

```php
Route::resource('/cars', CarsController::class);
```

In your commandline you can check your route list with:

```bash
php artisan route:list
```

You will get an overview of all your endpoint:

```bash
+--------+-----------+---------------------+---------+------------------------------------------------------------+------------------------------------------+
| Domain | Method    | URI                 | Name    | Action                                                     | Middleware                               |
+--------+-----------+---------------------+---------+------------------------------------------------------------+------------------------------------------+
|        | GET|HEAD  | /                   | index   | App\Http\Controllers\CarsController@index                  | web                                      |
|        | POST      | /                   | store   | App\Http\Controllers\CarsController@store                  | web                                      |
|        | GET|HEAD  | api/user            |         | Closure                                                    | api                                      |
|        |           |                     |         |                                                            | App\Http\Middleware\Authenticate:sanctum |
|        | GET|HEAD  | create              | create  | App\Http\Controllers\CarsController@create                 | web                                      |
|        | GET|HEAD  | sanctum/csrf-cookie |         | Laravel\Sanctum\Http\Controllers\CsrfCookieController@show | web                                      |
|        | GET|HEAD  | {}                  | show    | App\Http\Controllers\CarsController@show                   | web                                      |
|        | PUT|PATCH | {}                  | update  | App\Http\Controllers\CarsController@update                 | web                                      |
|        | DELETE    | {}                  | destroy | App\Http\Controllers\CarsController@destroy                | web                                      |
|        | GET|HEAD  | {}/edit             | edit    | App\Http\Controllers\CarsController@edit                   | web                                      |
+--------+-----------+---------------------+---------+------------------------------------------------------------+------------------------------------------+
```

Now you have an overview of all your endpoint('/')-methods of your
cars-controller (index, store, create, show, update, destroy, edit). Add a view
to your CarsController->index-method (views/cars/index.blade.php).

```php
 return view('cars.index');
```

Inside views add a folder called layouts and a file called app.blade.php.

```bash
.
├── cars
│   └── index.blade.php
└── layouts
    └── app.blade.php
```

In the app.blade.php place your code html-skel:
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <title>Cars</title>
    </head>
    <body>
        @yield('content')
    </body>
</html>
```

Extend it to the index view in the cars folder and add a content section:

```php
@extends('layouts.app')

@section('content')
    <h1>Cars</h1>
@endsection
```

