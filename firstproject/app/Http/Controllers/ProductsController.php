<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    // public function index()
    // {
    //     $title = "Welcome to the Jungle";
    //     $description = "Slash is an Idiot";
    //     $data = array('title' => 'Romeo and Juliet', 'description' => 'Song by Mark Knopfler');

    //     // compact method
    //     // return view('products/products', compact('title', 'description'));

    //     // with method is used to send one variable
    //     return view('products/products')->with('data', $data);
    // }


    // named routes
    public function index()
    {
        return view('products.index');
    }

    public function about()
    {
        return view('products/about');
    }

    public function show($id)
    {
        $data = array('1' => 'Zauberwürfel', '2' => 'Nintendo Switch');
        $toy = $data[$id] ?? 'Das Spielzeug mit der ID:' . $id . ' ist nicht vorhanden!';
        return view('products.toy')->with('toy', $toy);
    }

    public function showm($name, $id)
    {
        return $name . " - " . $id;
    }
}
