<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
// route sends back a view
Route::get('/', function () {
    return env('CREATOR_NAME');
});

// route sends back a string
Route::get('users/{id}', function ($id) {
    if ($id == 1) {
        $username = "Herr Guntler";
    } else {
        $username = "Not Herr Guntler";
    }
    return 'Users Page ' . $username . '!';
});

// route to users-Array(JSON)
Route::get('languages', function () {
    return ['PHP', 'HTML', 'Laravel'];
});

// route to courses returns JSON-object
Route::get('courses', function () {
    return response()->json([
        'name'=>'Herr Guntler',
        'course'=>'How to play guitar very slow'
    ]);
});

// route to return a function (that redirect the request to the index page)
Route::get('function/', function () {
    return redirect('/');
});
*/

// returns view
Route::get('', function () {
    return view('home');
});

// laravel 8 (new)
Route::get('products', [ProductsController::class, 'index'])->name('products');
Route::get('products/about', [ProductsController::class, 'about']);

// patterns (singl)
Route::get('products/{id}', [ProductsController::class, 'show'])->where('id', '[0-9]+');

// patterns (multiple)
Route::get('products/{name}/{id}', [ProductsController::class, 'showm'])->where([
    'name' => '[a-z]+',
    'id' => '[0-9]+'
]);

// laravel 8 (new)
// Route::get('products', 'App\Http\Controllers\ProductsController@index');
