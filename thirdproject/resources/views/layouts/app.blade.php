<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <title>Cars</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css" media="screen" title="no title" charset="utf-8">
    </head>
    <body>
        @yield('content')
    </body>
</html>
